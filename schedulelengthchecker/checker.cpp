#include "checker.h"
#include <stowplugininterface/problemadderinterface.h>
#include <stowplugininterface/idocument.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <stowplugininterface/problemadderinterface.h>
#include <stowplugininterface/problemlocation.h>
#include <stowplugininterface/problem.h>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;

Checker::Checker(QObject* parent): QObject(parent), m_document(0), m_problemAdder(0), m_warningLimit(20) {
    // Empty
}

Checker::~Checker() {
    // Empty
}

void Checker::setProblemAdder(ange::angelstow::ProblemAdderInterface* problemAdder) {
    m_problemAdder = problemAdder;
}



void Checker::reset(ange::angelstow::IDocument* newDocument) {
    if (m_document) {
        m_document->disconnect(this);
    }
    m_document = newDocument;
    rebuildCache();
}

void Checker::rebuildCache() {
    if(m_document->schedule()->size() > m_warningLimit) {
        Problem* problem = new ange::angelstow::Problem(Problem::Notice,ProblemLocation(), QStringLiteral("Schedule is long"));
        m_problem.reset(problem);
        m_problem->setDetails("If you shorten the schedule, the application might perform better");
        m_problemAdder->addProblem(problem);
    } else {
        m_problem.reset();
    }
}

void Checker::setWarningLimit(int i) {
    m_warningLimit = i; // for tests
}



Problem* Checker::problem() const {
    return m_problem.data();
}


#include "checker.moc"
