#include "schedulelengthcheckerplugin.h"
#include "checker.h"
#include <stowplugininterface/idocument.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

ScheduleLengthCheckerPlugin::ScheduleLengthCheckerPlugin(QObject* parent): QObject(parent), m_checker(new Checker(this)) {

}

void ScheduleLengthCheckerPlugin::setProblemAdder(ange::angelstow::ProblemAdderInterface* adder) {
    m_checker->setProblemAdder(adder);
}

void ScheduleLengthCheckerPlugin::documentReset(ange::angelstow::IDocument* document) {
    m_checker->reset(document);
    auto slot = [=]() {
        m_checker->rebuildCache();
    };
    connect(document->schedule(), &ange::schedule::Schedule::callAdded, slot);
    connect(document->schedule(), &ange::schedule::Schedule::callRemoved, slot);
    connect(document->schedule(), &ange::schedule::Schedule::rotationChanged, slot);
}

void ScheduleLengthCheckerPlugin::containerRemoved(const ange::containers::Container* container) {
    Q_UNUSED(container);
}

void ScheduleLengthCheckerPlugin::containerAdded(const ange::containers::Container* container) {
    Q_UNUSED(container);
}

void ScheduleLengthCheckerPlugin::containerChanged(const ange::containers::Container* container) {
    Q_UNUSED(container);
}


void ScheduleLengthCheckerPlugin::initializeReasonReserver(ange::angelstow::ReasonReserverInterface* reasonReserver) {
    Q_UNUSED(reasonReserver);
}

bool ScheduleLengthCheckerPlugin::legalAt(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about legality;
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
    return true;
}

QString ScheduleLengthCheckerPlugin::reasonToString(int reason) const {
    Q_UNUSED(reason);
    Q_ASSERT(false);
    return QString();
}

int ScheduleLengthCheckerPlugin::whyNot(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about legality;
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
    return 0;
}

void ScheduleLengthCheckerPlugin::containerMoved(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about container positions
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
}
