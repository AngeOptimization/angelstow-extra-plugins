#include <QObject>
#include <QScopedPointer>
#include <ange/containers/container.h>

namespace ange {
namespace angelstow {
class IDocument;
class Problem;
class ProblemAdderInterface;
}
}

class Checker : public QObject {
    Q_OBJECT
    public:
        Checker(QObject* parent = 0);
        /**
         * Resets the cache for a new document
         */
        void reset(ange::angelstow::IDocument* newDocument);
        /**
         * Sets the problem adder interface
         */
        void setProblemAdder(ange::angelstow::ProblemAdderInterface* problemAdder);
        /**
         * rebuilds the cache
         */
        void rebuildCache();
        virtual ~Checker();
        /**
         * For tests
         */
        ange::angelstow::Problem* problem() const;
        void setWarningLimit(int i);
    private:
        ange::angelstow::IDocument* m_document;
        ange::angelstow::ProblemAdderInterface* m_problemAdder;
        QScopedPointer<ange::angelstow::Problem> m_problem;
        int m_warningLimit;
};
