#include <QtTest>

#include <angelstow/test/documentloader.h>
#include <stowplugininterface/idocument.h>
#include <../schedulelengthcheckerplugin.h>
#include <../checker.h>
#include <angelstow/test/schedule_change_command.h>
#include <ange/schedule/schedule.h>
#include <angelstow/test/schedulechangerotationcommand.h>
#include <angelstow/test/containerchanger.h>

class TestScheduleLengthChecker : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleDocumentInsertCall();
        void testSimpleDocumentAppendCall();
        void testSimpleDocumentChangeRotation();
};

void TestScheduleLengthChecker::testSimpleDocumentChangeRotation() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../schedulelengthchecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first()));
    ScheduleLengthCheckerPlugin* plugin = dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first());
    plugin->checker()->setWarningLimit(6);
    plugin->checker()->rebuildCache();

    QVERIFY(!plugin->checker()->problem() );

    // try insert a call with the same uncode as the discharge port in the beginning
    schedule_change_command_t* addcommand = schedule_change_command_t::insert_call(1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    addcommand->redo();

    QCOMPARE(QString("Befor"),loader.idocument()->schedule()->at(0)->uncode() );
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(1)->uncode() );
    QCOMPARE(QString("Call0"),loader.idocument()->schedule()->at(2)->uncode() );
    QCOMPARE(QString("Call1"),loader.idocument()->schedule()->at(3)->uncode() );
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(4)->uncode() );
    QCOMPARE(QString("Call3"),loader.idocument()->schedule()->at(5)->uncode() );
    QCOMPARE(QString("After"),loader.idocument()->schedule()->at(6)->uncode() );

    QVERIFY(plugin->checker()->problem() );

    ScheduleChangeRotationCommand* changeRotationCommand = ScheduleChangeRotationCommand::changeOfRotation(loader.schedule(),1,2);
    changeRotationCommand->redo();

    QVERIFY(plugin->checker()->problem() );

    //undo the insertion and check that there no problems
    changeRotationCommand->undo();
    addcommand->undo();
    QVERIFY(!plugin->checker()->problem() );

    delete changeRotationCommand;
    delete addcommand;

}


void TestScheduleLengthChecker::testSimpleDocumentAppendCall() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../schedulelengthchecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);

    QVERIFY(dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first()));
    ScheduleLengthCheckerPlugin* plugin = dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first());
    plugin->checker()->setWarningLimit(6);
    plugin->checker()->rebuildCache();

    QVERIFY(!plugin->checker()->problem() );

    // try insert a call with the same uncode as the discharge port of the container after the discharge call
    schedule_change_command_t* command = schedule_change_command_t::insert_call(loader.idocument()->schedule()->size()-1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    QVERIFY(plugin->checker()->problem() );

    QCOMPARE(QString("Befor"),loader.idocument()->schedule()->at(0)->uncode());
    QCOMPARE(QString("Call0"),loader.idocument()->schedule()->at(1)->uncode());
    QCOMPARE(QString("Call1"),loader.idocument()->schedule()->at(2)->uncode());
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(3)->uncode());
    QCOMPARE(QString("Call3"),loader.idocument()->schedule()->at(4)->uncode());
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(5)->uncode());
    QCOMPARE(QString("After"),loader.idocument()->schedule()->at(6)->uncode());

    //undo the insertion and check that there still is no problems
    command->undo();
    QVERIFY(!plugin->checker()->problem() );

    delete command;
}


void TestScheduleLengthChecker::testSimpleDocumentInsertCall() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../schedulelengthchecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first()));
    ScheduleLengthCheckerPlugin* plugin = dynamic_cast<ScheduleLengthCheckerPlugin*>(loader.validatorPlugins().first());
    plugin->checker()->setWarningLimit(6);
    plugin->checker()->rebuildCache();


    QVERIFY(!plugin->checker()->problem() );

    // try insert a call with the same uncode as the discharge port of the container before the discharge port
    schedule_change_command_t* command = schedule_change_command_t::insert_call(2,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    // check that there is a problem now
    QVERIFY(plugin->checker()->problem() );

    //undo the insertion and check the problem is gone
    command->undo();
    QVERIFY(!plugin->checker()->problem() );

    delete command;
}


QTEST_GUILESS_MAIN(TestScheduleLengthChecker)
#include "testschedulelengthchecker.moc"
