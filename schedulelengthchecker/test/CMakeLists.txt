FIND_PACKAGE(Qt5Test 5.2.0 REQUIRED NO_MODULE)
FIND_PACKAGE(AngelstowTestInterface REQUIRED NO_MODULE)

include_directories(${CMAKE_CURRENT_BINARY_DIR}/..)

add_executable(test_schedulelengthchecker testschedulelengthchecker.cpp ../schedulelengthcheckerplugin.cpp ../checker.cpp)
target_link_libraries(test_schedulelengthchecker
                AngelstowTestInterface Ange::Vessel Ange::Containers Ange::Schedule
              Qt5::Test Ange::StowPluginInterface
             )
add_test(test_schedulelengthchecker test_schedulelengthchecker)
