#include "checker.h"
#include <stowplugininterface/idocument.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <stowplugininterface/problemadderinterface.h>
#include <stowplugininterface/problemlocation.h>
#include <stowplugininterface/problem.h>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;

Checker::Checker(QObject* parent): QObject(parent), m_document(0), m_problemAdder(0) {
    // Empty
}

Checker::~Checker() {
    // Empty
}

void Checker::setProblemAdder(ange::angelstow::ProblemAdderInterface* problemAdder) {
    m_problemAdder = problemAdder;
}


void Checker::checkContainer(const ange::containers::Container* container) {
    if (m_problemCache.contains(container)) {
        removeContainerFromCache(container);
    }
    const ange::schedule::Call* source = m_document->containerSource(container);
    const ange::schedule::Call* destination = m_document->containerDestination(container);
    QList<ange::schedule::Call*> passingPorts =m_document->schedule()->between(source,destination);
    QList<Problem*> problems;
    Q_FOREACH (const ange::schedule::Call* potential, passingPorts) {
        if (potential->uncode() == destination->uncode()) {
            ProblemLocation loc;
            loc.setCall(potential);

            Problem* problem = new Problem(Problem::Notice, loc,QStringLiteral("Container passes thru discharge port"));
            problem->setDetails(QStringLiteral("Container: %1").arg(container->equipmentNumber()));
            problems << problem;
            m_problemAdder->addProblem(problem);
        }
    }

    if (!problems.isEmpty()) {
        m_problemCache[container] = problems;
    }
}

void Checker::resetCache(ange::angelstow::IDocument* newDocument) {
    if (m_document) {
        m_document->disconnect(this);
    }
    m_document = newDocument;
    clearCache();
    buildCache();
}

void Checker::clearCache() {
    Q_FOREACH(QList<Problem*> problems, m_problemCache.values()) {
        qDeleteAll(problems);
    }
    m_problemCache.clear();
}

void Checker::removeContainerFromCache(const ange::containers::Container* container) {
    QList<ange::angelstow::Problem*> list = m_problemCache.take(container);
    qDeleteAll(list);
}

void Checker::buildCache() {
    Q_FOREACH (const ange::containers::Container* container, m_document->containers()) {
        checkContainer(container);
    }
}

QList< ange::angelstow::Problem* > Checker::problemsForContainer(const ange::containers::Container* container) const {
    return m_problemCache.value(container);
}

#include "checker.moc"
