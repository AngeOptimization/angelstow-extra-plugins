#include "containerroutecheckerplugin.h"

#include "checker.h"

#include <stowplugininterface/idocument.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

ContainerRouteCheckerPlugin::ContainerRouteCheckerPlugin(QObject* parent): QObject(parent), m_checker(new Checker(this)) {
    // Empty
}

void ContainerRouteCheckerPlugin::containerRemoved(const ange::containers::Container* container) {
    m_checker->removeContainerFromCache(container);
}

void ContainerRouteCheckerPlugin::containerAdded(const ange::containers::Container* container) {
    m_checker->checkContainer(container);
}

void ContainerRouteCheckerPlugin::containerChanged(const ange::containers::Container* container) {
    m_checker->checkContainer(container);
}

void ContainerRouteCheckerPlugin::setProblemAdder(ange::angelstow::ProblemAdderInterface* adder) {
    m_checker->setProblemAdder(adder);
}

void ContainerRouteCheckerPlugin::documentReset(ange::angelstow::IDocument* document) {
    m_checker->resetCache(document);
    auto slot = [=]() {
        m_checker->clearCache();
        m_checker->buildCache();
    };
    connect(document->schedule(), &ange::schedule::Schedule::callAdded, slot);
    connect(document->schedule(), &ange::schedule::Schedule::callRemoved, slot);
    connect(document->schedule(), &ange::schedule::Schedule::rotationChanged, slot);
}

void ContainerRouteCheckerPlugin::initializeReasonReserver(ange::angelstow::ReasonReserverInterface* reasonReserver) {
    Q_UNUSED(reasonReserver);
}

bool ContainerRouteCheckerPlugin::legalAt(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about legality;
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
    return true;
}

QString ContainerRouteCheckerPlugin::reasonToString(int reason) const {
    Q_UNUSED(reason);
    Q_ASSERT(false);
    return QString();
}

int ContainerRouteCheckerPlugin::whyNot(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about legality;
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
    return 0;
}

void ContainerRouteCheckerPlugin::containerMoved(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) {
    // we don't care about container positions
    Q_UNUSED(container);
    Q_UNUSED(call);
    Q_UNUSED(slot);
    Q_UNUSED(old_slot);
}
