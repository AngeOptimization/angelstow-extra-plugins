find_package(Qt5Test 5.2.0 REQUIRED CONFIG)
find_package(AngelstowTestInterface REQUIRED CONFIG)

add_executable(testcontainerchecker
    testcontainerchecker.cpp
)
target_link_libraries(testcontainerchecker
    containerroutechecker_notplugin
    Ange::Containers
    Ange::Schedule
    Ange::StowPluginInterface
    Ange::Vessel
    AngelstowTestInterface
    Qt5::Test
)
add_test(testcontainerchecker testcontainerchecker)
