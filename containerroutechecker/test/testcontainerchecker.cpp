#include <QtTest>

#include "checker.h"
#include "containerroutecheckerplugin.h"

#include <ange/schedule/schedule.h>
#include <angelstow/test/containerchanger.h>
#include <angelstow/test/documentloader.h>
#include <angelstow/test/schedule_change_command.h>
#include <angelstow/test/schedulechangerotationcommand.h>
#include <stowplugininterface/idocument.h>

class TestContainerChecker : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleDocumentInsertCall();
        void testSimpleDocumentAppendCall();
        void testSimpleDocumentChangeRotation();
        void testSimpleDocumentChangeContainerDischarge();
        void testSimpleDocumentChangeContainerLoad();
};

void TestContainerChecker::testSimpleDocumentChangeContainerLoad() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../containerroutechecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first()));
    ContainerRouteCheckerPlugin* plugin = dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first());

    QCOMPARE(loader.idocument()->containers().size(),1);

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    // try insert a call with the same uncode as the discharge port of the container after the discharge call
    schedule_change_command_t* command = schedule_change_command_t::insert_call(1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    QSharedPointer<ChangerResult> changer = ContainerChanger::changeLoadCall(loader.document(), loader.idocument()->containers().first(), loader.idocument()->schedule()->at(0));
    QUndoCommand* changerCommand = changer->takeCommand();

    changerCommand->redo();

    // check that there is a problem now
    QCOMPARE(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).length(),1);

    changerCommand->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    //undo the insertion and check that there still is no problems
    command->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    delete changerCommand;
    delete command;


}


void TestContainerChecker::testSimpleDocumentChangeContainerDischarge() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../containerroutechecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first()));
    ContainerRouteCheckerPlugin* plugin = dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first());

    QCOMPARE(loader.idocument()->containers().size(),1);

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    // try insert a call with the same uncode as the discharge port of the container after the discharge call
    schedule_change_command_t* command = schedule_change_command_t::insert_call(loader.idocument()->schedule()->size()-1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    QSharedPointer<ChangerResult> changer = ContainerChanger::changeDischargeCall(loader.document(), loader.idocument()->containers().first(), loader.idocument()->schedule()->at(5));
    QUndoCommand* changerCommand = changer->takeCommand();

    changerCommand->redo();

    // check that there is a problem now
    QCOMPARE(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).length(),1);

    changerCommand->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    //undo the insertion and check that there still is no problems
    command->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    delete changerCommand;
    delete command;

}


void TestContainerChecker::testSimpleDocumentChangeRotation() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../containerroutechecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first()));
    ContainerRouteCheckerPlugin* plugin = dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first());

    QCOMPARE(loader.idocument()->containers().size(),1);

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    // try insert a call with the same uncode as the discharge port in the beginning
    schedule_change_command_t* addcommand = schedule_change_command_t::insert_call(1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    addcommand->redo();

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    QCOMPARE(QString("Befor"),loader.idocument()->schedule()->at(0)->uncode() );
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(1)->uncode() );
    QCOMPARE(QString("Call0"),loader.idocument()->schedule()->at(2)->uncode() );
    QCOMPARE(QString("Call1"),loader.idocument()->schedule()->at(3)->uncode() );
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(4)->uncode() );
    QCOMPARE(QString("Call3"),loader.idocument()->schedule()->at(5)->uncode() );
    QCOMPARE(QString("After"),loader.idocument()->schedule()->at(6)->uncode() );

    ScheduleChangeRotationCommand* changeRotationCommand = ScheduleChangeRotationCommand::changeOfRotation(loader.schedule(),1,2);
    changeRotationCommand->redo();

    // check that there is a problem now
    QCOMPARE(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).length(),1);

    //undo the insertion and check that there no problems
    changeRotationCommand->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());
    addcommand->undo();

    delete changeRotationCommand;
    delete addcommand;

}


void TestContainerChecker::testSimpleDocumentAppendCall() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../containerroutechecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first()));
    ContainerRouteCheckerPlugin* plugin = dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first());

    QCOMPARE(loader.idocument()->containers().size(),1);

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    // try insert a call with the same uncode as the discharge port of the container after the discharge call
    schedule_change_command_t* command = schedule_change_command_t::insert_call(loader.idocument()->schedule()->size()-1,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    QCOMPARE(QString("Befor"),loader.idocument()->schedule()->at(0)->uncode());
    QCOMPARE(QString("Call0"),loader.idocument()->schedule()->at(1)->uncode());
    QCOMPARE(QString("Call1"),loader.idocument()->schedule()->at(2)->uncode());
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(3)->uncode());
    QCOMPARE(QString("Call3"),loader.idocument()->schedule()->at(4)->uncode());
    QCOMPARE(QString("Call2"),loader.idocument()->schedule()->at(5)->uncode());
    QCOMPARE(QString("After"),loader.idocument()->schedule()->at(6)->uncode());

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    //undo the insertion and check that there still is no problems
    command->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    delete command;
}


void TestContainerChecker::testSimpleDocumentInsertCall() {
    QFile f(QFINDTESTDATA("blank-with-one-container.sto"));
    QVERIFY(f.open(QIODevice::ReadOnly));
    DocumentLoader loader;
    bool result = loader.load(&f, "../containerroutechecker");
    QVERIFY(result);
    QCOMPARE(loader.validatorPlugins().size(), 1);
    QVERIFY(dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first()));
    ContainerRouteCheckerPlugin* plugin = dynamic_cast<ContainerRouteCheckerPlugin*>(loader.validatorPlugins().first());

    QCOMPARE(loader.idocument()->containers().size(),1);

    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    // try insert a call with the same uncode as the discharge port of the container before the discharge port
    schedule_change_command_t* command = schedule_change_command_t::insert_call(2,"Call2","TEST","Call2",QDateTime(), QDateTime(), 2, 30, ange::schedule::CraneRules::CraneRuleUnknown, ange::schedule::TwinLiftRules::TwinliftRuleUnknown, 30, loader.document(), QHash<const ange::vessel::VesselTank*, ange::units::Mass>());
    command->redo();

    // check that there is a problem now
    QCOMPARE(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).length(),1);

    //undo the insertion and check the problem is gone
    command->undo();
    QVERIFY(plugin->checker()->problemsForContainer(loader.idocument()->containers().first()).isEmpty());

    delete command;
}


QTEST_GUILESS_MAIN(TestContainerChecker)
#include "testcontainerchecker.moc"
