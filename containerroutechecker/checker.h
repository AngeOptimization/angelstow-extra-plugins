#include <QObject>
#include <ange/containers/container.h>

namespace ange {
namespace angelstow {
class IDocument;
class Problem;
class ProblemAdderInterface;
}
}

class Checker : public QObject {
    Q_OBJECT
    public:
        Checker(QObject* parent = 0);
        /**
         * Removes a container from the cache. Usually when the container is deleted
         */
        void removeContainerFromCache(const ange::containers::Container* container);
        /**
         * Checks and adds/remove the container from the cache. Either when the container
         * is newly added or the container somehow is changed
         */
        void checkContainer(const ange::containers::Container* container);
        /**
         * Resets the cache for a new document
         */
        void resetCache(ange::angelstow::IDocument* newDocument);
        /**
         * Sets the problem adder interface
         */
        void setProblemAdder(ange::angelstow::ProblemAdderInterface* problemAdder);
        /**
         * Clears the cache
         */
        void clearCache();
        /**
         * Builds the cache
         */
        virtual ~Checker();
        void buildCache();
        /**
         * For tests
         */
        QList<ange::angelstow::Problem*> problemsForContainer(const ange::containers::Container* container) const;
    private:
        ange::angelstow::IDocument* m_document;
        ange::angelstow::ProblemAdderInterface* m_problemAdder;
        QHash<const ange::containers::Container*, QList<ange::angelstow::Problem*>> m_problemCache;
};
