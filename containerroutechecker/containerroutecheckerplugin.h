#include <QObject>
#include <stowplugininterface/documentusinginterface.h>
#include <stowplugininterface/validatorplugin.h>
#include <stowplugininterface/problemaddinginterface.h>

class Checker;
namespace ange {
namespace angelstow {
class IDocument;
}
}

class ContainerRouteCheckerPlugin : public QObject, public ange::angelstow::DocumentUsingInterface, public ange::angelstow::ValidatorPlugin,
        public ange::angelstow::ProblemAddingInterface {
    Q_OBJECT
    Q_INTERFACES(ange::angelstow::DocumentUsingInterface ange::angelstow::ProblemAddingInterface ange::angelstow::ValidatorPlugin)
    Q_PLUGIN_METADATA(IID "dk.ange.angelstow.extras.containerroute" FILE "metadata.json")
    public:
        ContainerRouteCheckerPlugin(QObject* parent = 0);
        virtual void documentReset(ange::angelstow::IDocument* document) Q_DECL_OVERRIDE;
        virtual void initializeReasonReserver(ange::angelstow::ReasonReserverInterface* reasonReserver) Q_DECL_OVERRIDE;
        virtual void containerAdded(const ange::containers::Container* container) Q_DECL_OVERRIDE;
        virtual void containerChanged(const ange::containers::Container* container) Q_DECL_OVERRIDE;
        virtual void containerRemoved(const ange::containers::Container* container) Q_DECL_OVERRIDE;
        // the following are not used, but pure virtual in the validator plugin
        virtual void containerMoved(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) Q_DECL_OVERRIDE;
        virtual bool legalAt(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) Q_DECL_OVERRIDE;
        virtual QString reasonToString(int reason) const Q_DECL_OVERRIDE;
        virtual int whyNot(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) Q_DECL_OVERRIDE;
        virtual void setProblemAdder(ange::angelstow::ProblemAdderInterface* adder) Q_DECL_OVERRIDE;
        /**
         * Available for tests
         */
        const Checker* checker() const {
            return m_checker;
        }
    private:
        Checker* m_checker;
};
